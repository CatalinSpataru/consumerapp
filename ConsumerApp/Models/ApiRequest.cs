﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerApp.Models
{
    public class ApiRequest<T1, T2>
    {
        string ApiLink;
        List<Tuple<string, string>> PostData;
        Method Method;
        List<Tuple<string, string>> Headers;
        T2 Body;

        public ApiRequest(string apiLink, List<Tuple<string, string>> postData, T2 body, Method method, List<Tuple<string, string>> headers = null)
        {
            ApiLink = apiLink;
            PostData = postData != null ? postData : new List<Tuple<string, string>>();
            Headers = headers != null ? headers : new List<Tuple<string, string>>();
            Method = method;
            Body = body;
        }

        public async Task<T1> GetData()
        {
            var client = new RestClient(this.ApiLink);
            client.Timeout = -1;
            var request = new RestRequest(this.Method);

            foreach (var header in this.Headers)
            {
                request.AddHeader(header.Item1, header.Item2);
            }

            foreach (var postData in PostData)
            {
                request.AddParameter(postData.Item1, postData.Item2);
            }

            request.RequestFormat = DataFormat.Json;

            request.AddParameter("application/json", JsonConvert.SerializeObject(Body), ParameterType.RequestBody);

            try
            {
                IRestResponse response = await client.ExecuteAsync(request);

                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

                return JsonConvert.DeserializeObject<T1>(response.Content, jsonSerializerSettings);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

                return default;
            }
        }

    }
}
