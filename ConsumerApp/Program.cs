﻿using ConsumerApp.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConsumerApp
{
    class Program
    {
        private static string ApiLink = "https://localhost:44367/StringProcessor/";
        private static List<List<int>> DefaultConfiguration;
        private static Dictionary<int, string> ActionNames;

        static void Main(string[] args)
        {
            DefaultConfiguration = CreateDefaultConfiguration();
            ActionNames = CreateActions();

            string line;
            var file = new StreamReader(@"..\\..\\..\\In.txt");

            var stringsToProcess = new List<string>();

            while ((line = file.ReadLine()) != null)
            {
                stringsToProcess.Add(line);
            }

            ClearConfiguration();

            var initialize = new ApiRequest<string, List<string>>(ApiLink + "InitializeProcessor", null, stringsToProcess, RestSharp.Method.PUT, null);
            initialize.GetData().GetAwaiter().GetResult();

            Console.WriteLine("Configuration ready!");

            bool execute = true;

            while (execute)
            {
                int option;

                Console.WriteLine();

                Console.WriteLine("Select action:");
                Console.WriteLine("1 - Show current stages");
                Console.WriteLine("2 - Add to stage");
                Console.WriteLine("3 - Remove from stage");
                Console.WriteLine("4 - Use default configuration for stages");
                Console.WriteLine("5 - Start StringProcessor");
                Console.WriteLine("6 - Show result");
                Console.WriteLine("7 - Exit");

                Console.Write("Choice: ");
                string readLine = Console.ReadLine();

                int.TryParse(readLine, out option);

                Console.WriteLine();
                switch (option)
                {
                    case 1:
                        ShowCurrentConfiguration();
                        break;
                    case 2:
                        ShowCurrentConfiguration();
                        var addAction = AskWhereToAdd();
                        if (addAction == null)
                        {
                            Console.WriteLine("Exit action.");
                            break;
                        }
                        InsertAction(addAction.Item1, addAction.Item2);
                        break;
                    case 3:
                        ShowCurrentConfiguration();
                        var removeAction = AskWhatToRemove();
                        if (removeAction == null)
                        {
                            Console.WriteLine("Exit action.");
                            break;
                        }
                        RemoveAction(removeAction.Item1, removeAction.Item2);
                        break;
                    case 4:
                        PrintConfigurations(DefaultConfiguration);
                        AddDefaultConfiguration();
                        break;
                    case 5:
                        StartProcessing();
                        break;
                    case 6:
                        GetResult();
                        break;
                    case 7:
                        execute = false;
                        break;
                    default:
                        Console.WriteLine("Please select a number between 1-7");
                        break;
                }
            }
        }

        static List<List<int>> CreateDefaultConfiguration()
        {
            var createdList = new List<List<int>>();
            var stage1 = new List<int>();
            stage1.Add(0);
            var stage2 = new List<int>();
            stage2.Add(4); 
            var stage3 = new List<int>();
            stage3.Add(2);
            stage3.Add(1);

            createdList.Add(stage1);
            createdList.Add(stage2);
            createdList.Add(stage3);

            return createdList;
        }

        static Dictionary<int, string> CreateActions()
        {
            var dict = new Dictionary<int, string>()
            {
                { 0, "Lowercase" },
                { 1, "Uppercase" },
                { 2, "Sort" },
                { 3, "Invert" },
                { 4, "RemoveSpaces" }
            };

            return dict;
        }

        static Tuple<int, int> AskWhereToAdd()
        {
            int stage = -1, action = -1;
            Console.WriteLine();
            Console.WriteLine("Type \"-1\" if you want to exit");
            Console.WriteLine("Stage number (1-3) : ");
            Console.Write("Choice: ");
            string readLine = Console.ReadLine();
            Int32.TryParse(readLine, out stage);
            if (stage == -1)
                return null;
            stage -= 1;

            Console.WriteLine("Action that you want to add (0-4) : ");
            foreach(var actionName in ActionNames)
            {
                Console.WriteLine(actionName.Key + " - " + actionName.Value);
            }

            Console.WriteLine();
            Console.Write("Choice: ");
            readLine = Console.ReadLine();
            Int32.TryParse(readLine, out action);
            if (action == -1)
                return null;

            return new Tuple<int, int>(stage, action);
        }

        static Tuple<int, int> AskWhatToRemove()
        {
            int stage = -1, action = -1;
            Console.WriteLine("Type \"-1\" if you want to exit");
            Console.WriteLine("Stage number (1-3) : ");
            string readLine = Console.ReadLine();
            Int32.TryParse(readLine, out stage);
            if (stage == -1)
                return null;
            stage -= 1;

            foreach(var actionName in ActionNames)
            {
                Console.WriteLine(actionName.Key + " - " + actionName.Value);
            }
            Console.WriteLine("Action (from current stage) that you want removed (0-4) : ");
            readLine = Console.ReadLine();
            Int32.TryParse(readLine, out action);
            if (action == -1)
                return null;

            return new Tuple<int, int>(stage, action);
        }

        static void ShowCurrentConfiguration()
        {
            var request = new ApiRequest<List<List<int>>, string>(ApiLink + "GetStagesConfiguration", null, "", RestSharp.Method.GET, null);
            var result = request.GetData().GetAwaiter().GetResult();

            if (result != null)
                PrintConfigurations(result);
            else
                Console.WriteLine("No current configuration!");
        }

        static void PrintConfigurations(List<List<int>> configuration)
        {
            for (int index = 0; index < configuration.Count; index++)
            {
                Console.WriteLine("Stage " + (index + 1).ToString() + ":");
                foreach (var action in configuration[index])
                {
                    string actionName = "";
                    ActionNames.TryGetValue(action, out actionName);
                    Console.Write(actionName + " ");
                }
                Console.WriteLine();
            }
        }

        static void InsertAction(int stage, int action)
        {
            string link = ApiLink + "AddToStage?stage=" + stage.ToString() + "&action=" + action.ToString();

            var request = new ApiRequest<string, string>(link, null, "", RestSharp.Method.POST, null);
            var result = request.GetData().GetAwaiter().GetResult();
        }

        static void RemoveAction(int stage, int action)
        {
            string link = ApiLink + "RemoveFromStage?stage=" + stage.ToString() + "&action=" + action.ToString();

            var request = new ApiRequest<string, string>(link, null, "", RestSharp.Method.DELETE, null);
            var result = request.GetData().GetAwaiter().GetResult();
        }

        static void ClearConfiguration()
        {
            var request = new ApiRequest<string, string>(ApiLink + "RemoveAllStages", null, "", RestSharp.Method.DELETE, null);
            var result = request.GetData().GetAwaiter().GetResult();
        }

        static void AddDefaultConfiguration()
        {
            var request = new ApiRequest<string, List<List<int>>>(ApiLink + "AddAllStages", null, DefaultConfiguration, RestSharp.Method.POST, null);
            var result = request.GetData().GetAwaiter().GetResult();
        }

        static void StartProcessing()
        {
            var request = new ApiRequest<string, string>(ApiLink + "StartProcessing", null, "", RestSharp.Method.POST, null);
            var result = request.GetData().GetAwaiter().GetResult();

            Console.WriteLine(result);
        }

        static void GetResult()
        {
            var processedString = new ApiRequest<List<string>, string>(ApiLink + "GetResult", null, "", RestSharp.Method.GET, null);
            var result = processedString.GetData().GetAwaiter().GetResult();

            foreach (var str in result)
            {
                Console.WriteLine(str);
            }

            using (var streamWriter = new StreamWriter(@"..\\..\\..\\Out.txt"))
            {
                foreach (var str in result)
                {
                    streamWriter.WriteLine(str);
                }
            }
        }
    }
}
